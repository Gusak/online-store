﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormsApp.DTO.Product;
using WinFormsApp.DTO.User;

namespace WinFormsApp.DTO.OrderDetails
{
    public class OrderDetailsDto
    {
        public int UserId { get; set; }
        public ProductDto Product { get; set; }
        public int Quantity { get; set; }
    }
}
