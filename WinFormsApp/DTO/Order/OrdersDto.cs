﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormsApp.DTO.OrderDetails;

namespace WinFormsApp.DTO.Order
{
    public class OrdersDto
    {
        public int OrderId { get; set; }
        public IList<OrderDetailsDto> OrdersDetails { get; set; }

    }
}
