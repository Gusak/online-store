﻿using DataWebApi.DTO;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;

namespace DataWebApi.Components
{
    public class ServiceException : Exception
    {
        public static ServiceException Wrap(Exception ex, int errorCode = 0, string errorMessage = null, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            if (ex is WebException)
            {
                var webEx = ex as WebException;

                if (webEx.Response != null)
                {
                    using (var stream = webEx.Response.GetResponseStream())
                    {
                        using (var reader = new StreamReader(stream))
                        {
                            var json = reader.ReadToEnd();

                            if (!string.IsNullOrWhiteSpace(json))
                            {
                                var dto = JsonConvert.DeserializeObject<ServiceExceptionDto>(json);

                                var resp = webEx.Response as HttpWebResponse;

                                if (resp != null)
                                {
                                    statusCode = resp.StatusCode;
                                }

                                return new ServiceException(ex, dto.ErrorMessage, dto.ErrorCode, statusCode);
                            }
                        }
                    }
                }
            }

            return new ServiceException(ex, errorMessage, errorCode, statusCode);
        }

        public static ServiceException Wrap(Exception ex, ErrorType errorCode, string errorMessage = null, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            return Wrap(ex, (int)errorCode, errorMessage, statusCode);
        }

        public ServiceException(Exception ex = null, string errorMessage = null, int errorCode = 0, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            ErrorCode = errorCode;
            ErrorMessage = errorMessage;
            StatusCode = statusCode;
            Exception = ex;
        }

        public ServiceException(Exception ex = null, string errorMessage = null, ErrorType errorCode = 0, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            ErrorCode = (int)errorCode;
            ErrorMessage = errorMessage;
            StatusCode = statusCode;
            Exception = ex;
        }

        public string ErrorMessage { get; }
        public int ErrorCode { get; }
        public HttpStatusCode StatusCode { get; }
        public Exception Exception { get; }
    }
}
