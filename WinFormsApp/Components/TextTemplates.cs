﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsApp.Components
{
    public static class TextTemplates
    {
        public static readonly string ValidationMessageTemplate = "Validation message: {0}";
        public static readonly string LoggedInAsTemplate = "Logged in as {0}";
        public static readonly string OrderDescriptionTemplate = "Order #{0}";
        public static readonly string OrderDetailsTemplate = "Order #{0}: \r\n{1}In Total:{2}";
        public static readonly string ProductDescriptionTemplate = "Name: {0}, Amount: {1}, Price: {2} (Total price: {3})\r\n";
        public static readonly string UserNotFound = "User doesn't exist or you entered incorrect password";
    }
}
