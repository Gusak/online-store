﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WinFormsApp.Components
{
    public static class ErrorMessages
    {
        public readonly static string EmptyFieldMessage = "Please fill all fields!";
        public readonly static string UserNotFound = "User cannot be found! Please register new user.";
    }
}
