﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace WinFormsApp.Components
{
    public static class ConfigurationStorage
    {
        private static readonly IDictionary<string, string> Configurations;
        private static readonly string _configContent;
        static ConfigurationStorage()
        {
            try
            {
                _configContent = File.ReadAllText("configuration.json");
                Configurations = JsonConvert.DeserializeObject<IDictionary<string, string>>(_configContent);
            }
            catch
            {
                _configContent = string.Empty;
            }
        }

        public static string GetConfiguration(string configurationName)
        {  
            return Configurations.ContainsKey(configurationName) ? Configurations[configurationName] : string.Empty;
        }
    }
}
