﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataWebApi.Components
{
    public enum ErrorType
    {
        InternalError = 1000,
        UserNotFound = 1001,
        LoginFailed = 1002,
        UserAlreadyExist = 1003,
        RegistraionFailed = 2001,
    }
}
