﻿namespace WinFormsApp.Components
{
    public static class ApiServices
    {
        public readonly static string Login = "user/login";
        public readonly static string Registration = "user/register";
        public readonly static string GetAllProducts = "product/all";
        public readonly static string AddProduct = "product/add";
        public readonly static string UpdateProduct = "product/update";
        public readonly static string RemoveProduct = "product/remove";
        public readonly static string GetAllOrders = "order/all";
        public readonly static string MakeOrder = "order/add";
        public readonly static string RemoveOrder = "order/remove";
        public readonly static string RemoveAllOrders = "order/removeall";
    }
}
