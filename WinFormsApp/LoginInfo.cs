﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinFormsApp.DTO.User;

namespace WinFormsApp
{
    public static class LoginInfo
    {
        public static UserBaseInfo CurrentUser { get; private set;}

        public static void LoginUser(UserBaseInfo userInfo)
        {
            if (CurrentUser == null)
            {
                CurrentUser = userInfo;
            }
        }

        public static void LogoutUser()
        {
            CurrentUser = null;
        }

        public static string GetFullUserName()
        {
            return CurrentUser != null ? string.Format("{0} {1}", CurrentUser.Firstname, CurrentUser.Secondname) : string.Empty;
        }
    }
}
