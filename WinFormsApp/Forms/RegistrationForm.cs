﻿using DataWebApi.Components;
using System;
using System.Net.Http;
using System.Windows.Forms;
using WinFormsApp.Components;
using WinFormsApp.DTO.User;

namespace WinFormsApp.Forms
{
    public partial class RegistrationForm : Form
    {
        private MainForm _mainForm;
        private LoginForm _loginForm;

        public RegistrationForm()
        {
            InitializeComponent();
        }

        private void RegistrationButton_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(EmailTextBox.Text) ||
                string.IsNullOrEmpty(FirstNameTextBox.Text) ||
                string.IsNullOrEmpty(PasswordTextBox.Text) ||
                string.IsNullOrEmpty(LastNameTextBox.Text))
            {
                ValidationMessageLabel.Text = string.Format(TextTemplates.ValidationMessageTemplate, ErrorMessages.EmptyFieldMessage);
                ValidationMessageLabel.Show();
                return;
            }

            var request = new RegistrationDto
            {
                Email = EmailTextBox.Text,
                Firstname = FirstNameTextBox.Text,
                Password = PasswordTextBox.Text,
                Secondname = LastNameTextBox.Text
            };

            try
            {
                var result = RequestHelper.Execute<UserBaseInfo>(HttpMethod.Post, ApiServices.Registration, request);
                LoginInfo.LoginUser(request);
                if (_mainForm == null) _mainForm = new MainForm();
                _mainForm.Show();
            }
            catch(ServiceException sex)
            {
                // ToDo: Logger
                if (_loginForm == null) _loginForm = new LoginForm();
                _loginForm.Show();
            }

            Hide();
        }
    }
}
