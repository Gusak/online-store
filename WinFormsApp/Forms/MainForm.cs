﻿using DataWebApi.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Windows.Forms;
using WinFormsApp.Components;
using WinFormsApp.DTO.Order;
using WinFormsApp.DTO.OrderDetails;
using WinFormsApp.DTO.Product;

namespace WinFormsApp.Forms
{
    public partial class MainForm : Form
    {
        private LoginForm _loginForm;
        private List<ProductDto> _availableProducts;
        private List<OrderDetailsDto> _selectedProducts;
        private List<OrdersDto> _orders;
        private DataGridViewCell _cellToUpdate;

        public MainForm()
        {
            InitializeComponent();
            InitializeData();
        }

        private void LogoutButton_Click(object sender, EventArgs e)
        {
            LoginInfo.LogoutUser();
            Hide();
            if (_loginForm == null) _loginForm = new LoginForm();
            _loginForm.Show();
        }

        private void InitializeData()
        {
            StatusLabel.Text = string.Format(TextTemplates.LoggedInAsTemplate, LoginInfo.GetFullUserName());

            _availableProducts = new List<ProductDto>();
            _selectedProducts = new List<OrderDetailsDto>();
            _orders = new List<OrdersDto>();

            OfferedProductsDataGridView.Columns.Add("Name", "Name");
            OfferedProductsDataGridView.Columns.Add("Price", "Price");
            OfferedProductsDataGridView.Columns.Add("Remove", "Remove");
            OfferedProductsDataGridView.Columns.Add("Id", "Id");

            OfferedProductsDataGridView.Columns["Id"].Visible = false;
            OfferedProductsDataGridView.Columns["Remove"].Width = 30;
            OfferedProductsDataGridView.Columns["Remove"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            SelectedProductsDataGridView.Columns.Add("Name", "Name");
            SelectedProductsDataGridView.Columns.Add("Price", "Price");
            SelectedProductsDataGridView.Columns.Add("Quantity", "Quantity");
            SelectedProductsDataGridView.Columns.Add("Inc", "Inc");
            SelectedProductsDataGridView.Columns.Add("Dec", "Dec");
            SelectedProductsDataGridView.Columns.Add("Id", "Id");

            SelectedProductsDataGridView.Columns["Inc"].Width = 30;
            SelectedProductsDataGridView.Columns["Dec"].Width = 30;
            SelectedProductsDataGridView.Columns["Inc"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            SelectedProductsDataGridView.Columns["Dec"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            SelectedProductsDataGridView.Columns["Id"].Visible = false;

            FillAvailableProductDataGrid();

            FetchUserOrders();
            ShowDetails();
        }

        private void FillSelectedProductDataGrid()
        {
            SelectedProductsDataGridView.Rows.Clear();
            foreach (var item in _selectedProducts)
            {
                var row = new DataGridViewRow();
                var nameCell = new DataGridViewTextBoxCell();
                var priceCell = new DataGridViewTextBoxCell();
                var quantityCell = new DataGridViewTextBoxCell();
                var plusCell = new DataGridViewButtonCell();
                var minusCell = new DataGridViewButtonCell();
                var indexCell = new DataGridViewTextBoxCell();

                nameCell.Value = item.Product.Name;
                priceCell.Value = item.Product.Price;
                quantityCell.Value = item.Quantity;
                plusCell.Value = "+";
                minusCell.Value = "-";
                indexCell.Value = item.Product.Id;

                row.Cells.Insert(0, nameCell);
                row.Cells.Insert(1, priceCell);
                row.Cells.Insert(2, quantityCell);
                row.Cells.Insert(3, plusCell);
                row.Cells.Insert(4, minusCell);
                row.Cells.Insert(5, indexCell);

                SelectedProductsDataGridView.Rows.Add(row);
            }
        }

        private void FillAvailableProductDataGrid()
        {
            _availableProducts = GetAllProducts();
            OfferedProductsDataGridView.Rows.Clear();
            foreach (var item in _availableProducts)
            {
                var row = new DataGridViewRow();
                var nameCell = new DataGridViewTextBoxCell();
                var priceCell = new DataGridViewTextBoxCell();
                var deleteCell = new DataGridViewButtonCell();
                var indexCell = new DataGridViewButtonCell();

                nameCell.Value = item.Name;
                priceCell.Value = item.Price;
                deleteCell.Value = "x";
                indexCell.Value = item.Id;

                row.Cells.Insert(0, nameCell);
                row.Cells.Insert(1, priceCell);
                row.Cells.Insert(2, deleteCell);
                row.Cells.Insert(3, indexCell);

                OfferedProductsDataGridView.Rows.Add(row);
            }
        }

        private List<ProductDto> GetAllProducts()
        {
            try
            {
                return RequestHelper.Execute<List<ProductDto>>(HttpMethod.Get, ApiServices.GetAllProducts);
            }
            catch(ServiceException sex)
            {
                // ToDo: Logger
                return Enumerable.Empty<ProductDto>().ToList();
            }
        }

        private void FetchUserOrders()
        {
            OrdersListBox.Items.Clear();
            try
            {
                _orders = RequestHelper.Execute<List<OrdersDto>>(HttpMethod.Post, ApiServices.GetAllOrders, LoginInfo.CurrentUser.Id);
                foreach (var order in _orders)
                {
                    OrdersListBox.Items.Add(string.Format(TextTemplates.OrderDescriptionTemplate, order.OrderId));
                }
            }
            catch (ServiceException sex)
            {
                // ToDo: Logger
            }
        }

        private int GetSelectedOrderIndex()
        {
            if (OrdersListBox.SelectedItems.Count > 0)
            {
                var selectedValue = OrdersListBox.SelectedItems[0] as string;
                if (!string.IsNullOrEmpty(selectedValue))
                {
                    int orderId;
                    return int.TryParse(selectedValue.Substring(selectedValue.IndexOf('#') + 1), out orderId) ? orderId : -1;
                }
            }
            return -1;

        }

        private void ShowDetails()
        {
            OrderDetailsTextBox.Text = string.Empty;
            var orderId = GetSelectedOrderIndex();
            if (orderId > -1)
            {
                var details = _orders.FirstOrDefault(c => c.OrderId.Equals(orderId));
                if (details != null && details.OrdersDetails.Count > 0)
                {
                    var totalSum = details.OrdersDetails.Sum(c => c.Product.Price * c.Quantity);
                    string productsDetails = string.Empty;
                    foreach (var detail in details.OrdersDetails)
                    {
                        productsDetails += string.Format(TextTemplates.ProductDescriptionTemplate, detail.Product.Name, detail.Quantity, detail.Product.Price, detail.Quantity * detail.Product.Price);
                    }
                    OrderDetailsTextBox.Text = string.Format(TextTemplates.OrderDetailsTemplate, details.OrderId, productsDetails, totalSum);
                }
            }
        }

        private void AddOrUpdateProduct(ProductDto product)
        {
            var result = _availableProducts.FirstOrDefault(s=> s.Name == product.Name || s.Price == product.Price);

            if(result != null) product.Id = result.Id;

            try
            {
                if(result != null)
                {
                    RequestHelper.Execute(HttpMethod.Put, ApiServices.UpdateProduct, product);
                }
                else
                {
                    RequestHelper.Execute(HttpMethod.Post, ApiServices.AddProduct, product);
                }
            }
            catch(ServiceException ex)
            {
                // ToDo: Logger
            }
        }

        private void AddProductButton_Click(object sender, EventArgs e)
        {
            var rowIndex = OfferedProductsDataGridView.SelectedCells[0].RowIndex;
            var name = OfferedProductsDataGridView["Name", rowIndex].Value;
            var price = OfferedProductsDataGridView["Price", rowIndex].Value;
            var id = OfferedProductsDataGridView["Id", rowIndex].Value;
            var quantity = ProductQuantityNumericUpDown.Value;

            var index = _selectedProducts.FindIndex(c => c.Product.Name.Equals(name));

            if (index > -1)
            {
                _selectedProducts[index].Quantity = _selectedProducts[index].Quantity + (int)quantity;
            }
            else
            {
                _selectedProducts.Add(new OrderDetailsDto
                {
                    Product = new ProductDto
                    {
                        Name = name?.ToString(),
                        Price = (decimal)price,
                        Id = (int)id },
                        Quantity = (int)quantity,
                        UserId = LoginInfo.CurrentUser.Id
                    }
                );
            }

            FillSelectedProductDataGrid();
        }

        private void RemoveProductButton_Click(object sender, EventArgs e)
        {
            if (SelectedProductsDataGridView.Rows.Count > 0)
            {
                var selectedIndex = SelectedProductsDataGridView.SelectedRows[0].Index;

                var productName = SelectedProductsDataGridView.SelectedRows[0].Cells["Name"].Value;

                _selectedProducts.RemoveAll(c => c.Product.Name.Equals(productName));

                FillSelectedProductDataGrid();

                var newSelectedIndex = SelectedProductsDataGridView.Rows.Count - 1 < selectedIndex ? --selectedIndex : selectedIndex;
                foreach (DataGridViewRow row in SelectedProductsDataGridView.Rows)
                {
                    if (row.Index == newSelectedIndex)
                    {
                        row.Selected = true;
                    }  
                }
            }
        }

        private void SelectedProductsDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (e.RowIndex >= 0 && (e.ColumnIndex == senderGrid.Columns["Inc"].Index || e.ColumnIndex == senderGrid.Columns["Dec"].Index))
            {
                var quantity = senderGrid.Rows[e.RowIndex].Cells["Quantity"].Value as int? ?? 1;
                var index = _selectedProducts.FindIndex(c => c.Product.Name.Equals(senderGrid.Rows[e.RowIndex].Cells["Name"].Value));

                if (index == -1) return;

                if (e.ColumnIndex == senderGrid.Columns["Inc"].Index)
                {
                    ++quantity;
                    _selectedProducts[index].Quantity++;
                }
                else
                {
                    quantity = quantity > 1 ? --quantity : quantity;
                    _selectedProducts[index].Quantity--;
                }

                senderGrid.Rows[e.RowIndex].Cells["Quantity"].Value = quantity;
            }
        }

        private void MakeOrderButton_Click(object sender, EventArgs e)
        {
            try
            {
                RequestHelper.Execute(HttpMethod.Post, ApiServices.MakeOrder, _selectedProducts);
                FetchUserOrders();
                ShowDetails();
            }
            catch(ServiceException ex)
            {
                // ToDo: Logger
            }
        }

        private void OrdersListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowDetails();
        }

        private void RemoveOrderButton_Click(object sender, EventArgs e)
        {
            var orderId = GetSelectedOrderIndex();
            if (orderId > -1)
            {
                try
                {
                    RequestHelper.Execute(HttpMethod.Delete, ApiServices.RemoveOrder, orderId);
                    FetchUserOrders();
                    ShowDetails();
                }
                catch (ServiceException ex)
                {
                    // ToDo: Logger }
                }
            }
        }

        private void RemoveAllOrdersButton_Click(object sender, EventArgs e)
        {
            try
            {
                RequestHelper.Execute(HttpMethod.Delete, ApiServices.RemoveAllOrders, LoginInfo.CurrentUser.Id);
                FetchUserOrders();
                ShowDetails();
            }
            catch (ServiceException ex)
            {
                // ToDo: Logger
            }
        }

        private void OfferedProductsDataGridView_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            e.Control.KeyPress -= new KeyPressEventHandler(NumericColumn_KeyPress);
            if (senderGrid.CurrentCell.ColumnIndex == 1)
            {  
                var textBox = e.Control as TextBox;
                if (textBox != null)
                {
                    textBox.KeyPress += new KeyPressEventHandler(NumericColumn_KeyPress);
                }
            }
        }

        private void NumericColumn_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void OfferedProductsDataGridView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            var cellToCheck = e.ColumnIndex == senderGrid.Columns["Name"].Index ? senderGrid.Columns["Price"].Index : senderGrid.Columns["Name"].Index;
            var currentValue = senderGrid[cellToCheck, e.RowIndex].Value;
            if (currentValue == null || (currentValue is decimal && (decimal)currentValue == 0))
            {
                _cellToUpdate = senderGrid[cellToCheck, e.RowIndex];
            }
        }

        private void OfferedProductsDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            if (_cellToUpdate != null)
            {
                OfferedProductsDataGridView.CurrentCell = _cellToUpdate;
                OfferedProductsDataGridView.BeginEdit(true);
                _cellToUpdate = null;
            }
        }

        private void OfferedProductsDataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            var product = new ProductDto();
            var senderGrid = (DataGridView)sender;
            var currentCell = senderGrid[e.ColumnIndex, e.RowIndex];
            if (e.ColumnIndex == OfferedProductsDataGridView.Columns["Name"].Index)
            {
                var nextCell = senderGrid["Price", e.RowIndex];

                if (!string.IsNullOrEmpty((string)currentCell.Value) && nextCell.Value != null && nextCell.Value is decimal && (decimal)nextCell.Value > 0)
                {
                    product.Name = (string)currentCell.Value;
                    product.Price = (decimal)nextCell.Value;

                    AddOrUpdateProduct(product);

                    FillAvailableProductDataGrid();
                }
                
            }
            else if(e.ColumnIndex == OfferedProductsDataGridView.Columns["Price"].Index)
            {
                var previousCell = senderGrid["Name", e.RowIndex];

                decimal decValue;
                if (!string.IsNullOrEmpty((string)previousCell.Value) && decimal.TryParse((string)currentCell.Value, out decValue) && decValue > 0)
                {
                    product.Name = (string)previousCell.Value;
                    product.Price = decValue;

                    AddOrUpdateProduct(product);

                    FillAvailableProductDataGrid();
                }
            }
            
        }

        private void OfferedProductsDataGridView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            var senderGrid = (DataGridView)sender;
            var oldValue = senderGrid[e.ColumnIndex, e.RowIndex].Value;
            var newValue = e.FormattedValue;

            if ((e.ColumnIndex == senderGrid.Columns["Name"].Index && string.IsNullOrEmpty((string)newValue)) ||
                (e.ColumnIndex == senderGrid.Columns["Price"].Index && (string.IsNullOrEmpty((string)newValue) || (string)newValue == "0") && oldValue != null && (decimal)oldValue > 0))
            {
                senderGrid.CancelEdit();
            }
        }

        private void OfferedProductsDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (e.RowIndex >= 0 && e.ColumnIndex == OfferedProductsDataGridView.Columns["Remove"].Index)
            {
                var row = senderGrid.Rows[e.RowIndex];
                if (row != null)
                {
                    var productToRemove = new ProductDto
                    {
                        Name = (string)row.Cells["Name"].Value,
                        Price = (decimal)row.Cells["Price"].Value,
                        Id = (int)row.Cells["Id"].Value
                    };

                    try
                    {
                        RequestHelper.Execute(HttpMethod.Delete, ApiServices.RemoveProduct, productToRemove);
                        FillAvailableProductDataGrid();
                        FetchUserOrders();
                        FillSelectedProductDataGrid();
                    }
                    catch (ServiceException ex)
                    {
                        // ToDo: Logger
                    }
                }
            }
        }

        private void OfferedProductsDataGridView_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            OfferedProductsDataGridView.SelectionChanged += new EventHandler(OfferedProductsDataGridView_SelectionChanged);
            
            if (OfferedProductsDataGridView.Rows[e.RowIndex].Cells.Count >= 2)
            {
                var deleteCell = new DataGridViewButtonCell();
                deleteCell.Value = "x";
                OfferedProductsDataGridView["Remove", e.RowIndex] = deleteCell;
            }
        }
    }
}
