﻿using DataWebApi.Components;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Windows.Forms;
using WinFormsApp.Components;
using WinFormsApp.DTO.User;

namespace WinFormsApp.Forms
{
    public partial class LoginForm : Form
    {
        private MainForm _mainForm;
        private RegistrationForm _registrationForm;

        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(EmailTextBox.Text) || string.IsNullOrWhiteSpace(PasswordTextBox.Text))
            {
                ValidationMessageLabel.Text = string.Format(TextTemplates.ValidationMessageTemplate, ErrorMessages.EmptyFieldMessage);
                ValidationMessageLabel.Show();
                return;
            }

            var request = new LoginDto { Email = EmailTextBox.Text, Password = PasswordTextBox.Text };

            UserBaseInfo result = null;
            try
            {
                result = RequestHelper.Execute<UserBaseInfo>(HttpMethod.Post, ApiServices.Login, request);
            }
            catch (ServiceException ex)
            {
                // ToDo: Logger
            }

            if (result != null)
            {
                LoginInfo.LoginUser(result);
                if (_mainForm == null) _mainForm = new MainForm();
                _mainForm.Show();
                Hide();
            }
            else
            {
                ValidationMessageLabel.Text = string.Format(TextTemplates.UserNotFound, ErrorMessages.EmptyFieldMessage);
                ValidationMessageLabel.Show();
            }
        }

        private void RegistrationLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (_registrationForm == null) _registrationForm = new RegistrationForm();
            _registrationForm.Show();
            Hide();
        }
    }
}
