﻿namespace WinFormsApp.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.StatusLabel = new System.Windows.Forms.Label();
            this.ProductGroupBox = new System.Windows.Forms.GroupBox();
            this.BasketLabel = new System.Windows.Forms.Label();
            this.AvailableProdcutsLabel = new System.Windows.Forms.Label();
            this.SelectedProductsDataGridView = new System.Windows.Forms.DataGridView();
            this.OfferedProductsDataGridView = new System.Windows.Forms.DataGridView();
            this.ProductQuantityLabel = new System.Windows.Forms.Label();
            this.ProductQuantityNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.MakeOrderButton = new System.Windows.Forms.Button();
            this.RemoveProductButton = new System.Windows.Forms.Button();
            this.AddProductButton = new System.Windows.Forms.Button();
            this.OrdersGroupBox = new System.Windows.Forms.GroupBox();
            this.OrderDetailsTextBox = new System.Windows.Forms.TextBox();
            this.RemoveAllOrdersButton = new System.Windows.Forms.Button();
            this.RemoveOrderButton = new System.Windows.Forms.Button();
            this.OrdersListBox = new System.Windows.Forms.ListBox();
            this.LogoutButton = new System.Windows.Forms.Button();
            this.ProductGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedProductsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OfferedProductsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProductQuantityNumericUpDown)).BeginInit();
            this.OrdersGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // StatusLabel
            // 
            this.StatusLabel.AutoSize = true;
            this.StatusLabel.Location = new System.Drawing.Point(12, 9);
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(68, 13);
            this.StatusLabel.TabIndex = 0;
            this.StatusLabel.Text = "Logged in as";
            // 
            // ProductGroupBox
            // 
            this.ProductGroupBox.Controls.Add(this.BasketLabel);
            this.ProductGroupBox.Controls.Add(this.AvailableProdcutsLabel);
            this.ProductGroupBox.Controls.Add(this.SelectedProductsDataGridView);
            this.ProductGroupBox.Controls.Add(this.OfferedProductsDataGridView);
            this.ProductGroupBox.Controls.Add(this.ProductQuantityLabel);
            this.ProductGroupBox.Controls.Add(this.ProductQuantityNumericUpDown);
            this.ProductGroupBox.Controls.Add(this.MakeOrderButton);
            this.ProductGroupBox.Controls.Add(this.RemoveProductButton);
            this.ProductGroupBox.Controls.Add(this.AddProductButton);
            this.ProductGroupBox.Location = new System.Drawing.Point(15, 42);
            this.ProductGroupBox.Name = "ProductGroupBox";
            this.ProductGroupBox.Size = new System.Drawing.Size(557, 418);
            this.ProductGroupBox.TabIndex = 4;
            this.ProductGroupBox.TabStop = false;
            this.ProductGroupBox.Text = "Products";
            // 
            // BasketLabel
            // 
            this.BasketLabel.AutoSize = true;
            this.BasketLabel.Location = new System.Drawing.Point(319, 20);
            this.BasketLabel.Name = "BasketLabel";
            this.BasketLabel.Size = new System.Drawing.Size(67, 13);
            this.BasketLabel.TabIndex = 14;
            this.BasketLabel.Text = "Your basket:";
            // 
            // AvailableProdcutsLabel
            // 
            this.AvailableProdcutsLabel.AutoSize = true;
            this.AvailableProdcutsLabel.Location = new System.Drawing.Point(21, 24);
            this.AvailableProdcutsLabel.Name = "AvailableProdcutsLabel";
            this.AvailableProdcutsLabel.Size = new System.Drawing.Size(97, 13);
            this.AvailableProdcutsLabel.TabIndex = 13;
            this.AvailableProdcutsLabel.Text = "Available products:";
            // 
            // SelectedProductsDataGridView
            // 
            this.SelectedProductsDataGridView.AllowUserToAddRows = false;
            this.SelectedProductsDataGridView.AllowUserToDeleteRows = false;
            this.SelectedProductsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.SelectedProductsDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.SelectedProductsDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.SelectedProductsDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.SelectedProductsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.SelectedProductsDataGridView.DefaultCellStyle = dataGridViewCellStyle1;
            this.SelectedProductsDataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.SelectedProductsDataGridView.EnableHeadersVisualStyles = false;
            this.SelectedProductsDataGridView.Location = new System.Drawing.Point(319, 43);
            this.SelectedProductsDataGridView.MultiSelect = false;
            this.SelectedProductsDataGridView.Name = "SelectedProductsDataGridView";
            this.SelectedProductsDataGridView.ReadOnly = true;
            this.SelectedProductsDataGridView.RowHeadersVisible = false;
            this.SelectedProductsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.SelectedProductsDataGridView.Size = new System.Drawing.Size(221, 251);
            this.SelectedProductsDataGridView.TabIndex = 12;
            this.SelectedProductsDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.SelectedProductsDataGridView_CellContentClick);
            // 
            // OfferedProductsDataGridView
            // 
            this.OfferedProductsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.OfferedProductsDataGridView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.OfferedProductsDataGridView.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.OfferedProductsDataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.OfferedProductsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.OfferedProductsDataGridView.DefaultCellStyle = dataGridViewCellStyle2;
            this.OfferedProductsDataGridView.EnableHeadersVisualStyles = false;
            this.OfferedProductsDataGridView.Location = new System.Drawing.Point(21, 43);
            this.OfferedProductsDataGridView.MultiSelect = false;
            this.OfferedProductsDataGridView.Name = "OfferedProductsDataGridView";
            this.OfferedProductsDataGridView.RowHeadersVisible = false;
            this.OfferedProductsDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.OfferedProductsDataGridView.Size = new System.Drawing.Size(221, 251);
            this.OfferedProductsDataGridView.TabIndex = 11;
            this.OfferedProductsDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.OfferedProductsDataGridView_CellContentClick);
            this.OfferedProductsDataGridView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.OfferedProductsDataGridView_CellEndEdit);
            this.OfferedProductsDataGridView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.OfferedProductsDataGridView_CellValidating);
            this.OfferedProductsDataGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.OfferedProductsDataGridView_CellValueChanged);
            this.OfferedProductsDataGridView.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.OfferedProductsDataGridView_EditingControlShowing);
            this.OfferedProductsDataGridView.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.OfferedProductsDataGridView_RowsAdded);
            // 
            // ProductQuantityLabel
            // 
            this.ProductQuantityLabel.AutoSize = true;
            this.ProductQuantityLabel.Location = new System.Drawing.Point(18, 317);
            this.ProductQuantityLabel.Name = "ProductQuantityLabel";
            this.ProductQuantityLabel.Size = new System.Drawing.Size(84, 13);
            this.ProductQuantityLabel.TabIndex = 9;
            this.ProductQuantityLabel.Text = "Product quantity";
            // 
            // ProductQuantityNumericUpDown
            // 
            this.ProductQuantityNumericUpDown.Location = new System.Drawing.Point(21, 333);
            this.ProductQuantityNumericUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ProductQuantityNumericUpDown.Name = "ProductQuantityNumericUpDown";
            this.ProductQuantityNumericUpDown.Size = new System.Drawing.Size(81, 20);
            this.ProductQuantityNumericUpDown.TabIndex = 8;
            this.ProductQuantityNumericUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // MakeOrderButton
            // 
            this.MakeOrderButton.Location = new System.Drawing.Point(21, 375);
            this.MakeOrderButton.Name = "MakeOrderButton";
            this.MakeOrderButton.Size = new System.Drawing.Size(134, 23);
            this.MakeOrderButton.TabIndex = 7;
            this.MakeOrderButton.Text = "Make order";
            this.MakeOrderButton.UseVisualStyleBackColor = true;
            this.MakeOrderButton.Click += new System.EventHandler(this.MakeOrderButton_Click);
            // 
            // RemoveProductButton
            // 
            this.RemoveProductButton.Location = new System.Drawing.Point(248, 157);
            this.RemoveProductButton.Name = "RemoveProductButton";
            this.RemoveProductButton.Size = new System.Drawing.Size(65, 23);
            this.RemoveProductButton.TabIndex = 6;
            this.RemoveProductButton.Text = "Remove";
            this.RemoveProductButton.UseVisualStyleBackColor = true;
            this.RemoveProductButton.Click += new System.EventHandler(this.RemoveProductButton_Click);
            // 
            // AddProductButton
            // 
            this.AddProductButton.Location = new System.Drawing.Point(248, 114);
            this.AddProductButton.Name = "AddProductButton";
            this.AddProductButton.Size = new System.Drawing.Size(65, 23);
            this.AddProductButton.TabIndex = 5;
            this.AddProductButton.Text = "Add";
            this.AddProductButton.UseVisualStyleBackColor = true;
            this.AddProductButton.Click += new System.EventHandler(this.AddProductButton_Click);
            // 
            // OrdersGroupBox
            // 
            this.OrdersGroupBox.Controls.Add(this.OrderDetailsTextBox);
            this.OrdersGroupBox.Controls.Add(this.RemoveAllOrdersButton);
            this.OrdersGroupBox.Controls.Add(this.RemoveOrderButton);
            this.OrdersGroupBox.Controls.Add(this.OrdersListBox);
            this.OrdersGroupBox.Location = new System.Drawing.Point(578, 42);
            this.OrdersGroupBox.Name = "OrdersGroupBox";
            this.OrdersGroupBox.Size = new System.Drawing.Size(429, 418);
            this.OrdersGroupBox.TabIndex = 5;
            this.OrdersGroupBox.TabStop = false;
            this.OrdersGroupBox.Text = "Orders";
            // 
            // OrderDetailsTextBox
            // 
            this.OrderDetailsTextBox.Location = new System.Drawing.Point(20, 186);
            this.OrderDetailsTextBox.Multiline = true;
            this.OrderDetailsTextBox.Name = "OrderDetailsTextBox";
            this.OrderDetailsTextBox.ReadOnly = true;
            this.OrderDetailsTextBox.Size = new System.Drawing.Size(392, 212);
            this.OrderDetailsTextBox.TabIndex = 3;
            // 
            // RemoveAllOrdersButton
            // 
            this.RemoveAllOrdersButton.Location = new System.Drawing.Point(264, 81);
            this.RemoveAllOrdersButton.Name = "RemoveAllOrdersButton";
            this.RemoveAllOrdersButton.Size = new System.Drawing.Size(120, 23);
            this.RemoveAllOrdersButton.TabIndex = 2;
            this.RemoveAllOrdersButton.Text = "Remove all orders";
            this.RemoveAllOrdersButton.UseVisualStyleBackColor = true;
            this.RemoveAllOrdersButton.Click += new System.EventHandler(this.RemoveAllOrdersButton_Click);
            // 
            // RemoveOrderButton
            // 
            this.RemoveOrderButton.Location = new System.Drawing.Point(264, 43);
            this.RemoveOrderButton.Name = "RemoveOrderButton";
            this.RemoveOrderButton.Size = new System.Drawing.Size(120, 23);
            this.RemoveOrderButton.TabIndex = 1;
            this.RemoveOrderButton.Text = "Remove order";
            this.RemoveOrderButton.UseVisualStyleBackColor = true;
            this.RemoveOrderButton.Click += new System.EventHandler(this.RemoveOrderButton_Click);
            // 
            // OrdersListBox
            // 
            this.OrdersListBox.FormattingEnabled = true;
            this.OrdersListBox.Location = new System.Drawing.Point(20, 43);
            this.OrdersListBox.Name = "OrdersListBox";
            this.OrdersListBox.Size = new System.Drawing.Size(208, 121);
            this.OrdersListBox.TabIndex = 0;
            this.OrdersListBox.SelectedIndexChanged += new System.EventHandler(this.OrdersListBox_SelectedIndexChanged);
            // 
            // LogoutButton
            // 
            this.LogoutButton.Location = new System.Drawing.Point(932, 9);
            this.LogoutButton.Name = "LogoutButton";
            this.LogoutButton.Size = new System.Drawing.Size(75, 23);
            this.LogoutButton.TabIndex = 6;
            this.LogoutButton.Text = "Logout";
            this.LogoutButton.UseVisualStyleBackColor = true;
            this.LogoutButton.Click += new System.EventHandler(this.LogoutButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1023, 480);
            this.Controls.Add(this.LogoutButton);
            this.Controls.Add(this.OrdersGroupBox);
            this.Controls.Add(this.ProductGroupBox);
            this.Controls.Add(this.StatusLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MainForm";
            this.ProductGroupBox.ResumeLayout(false);
            this.ProductGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedProductsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OfferedProductsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ProductQuantityNumericUpDown)).EndInit();
            this.OrdersGroupBox.ResumeLayout(false);
            this.OrdersGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label StatusLabel;
        private System.Windows.Forms.GroupBox ProductGroupBox;
        private System.Windows.Forms.Button RemoveProductButton;
        private System.Windows.Forms.Button AddProductButton;
        private System.Windows.Forms.Label ProductQuantityLabel;
        private System.Windows.Forms.NumericUpDown ProductQuantityNumericUpDown;
        private System.Windows.Forms.Button MakeOrderButton;
        private System.Windows.Forms.GroupBox OrdersGroupBox;
        private System.Windows.Forms.TextBox OrderDetailsTextBox;
        private System.Windows.Forms.Button RemoveAllOrdersButton;
        private System.Windows.Forms.Button RemoveOrderButton;
        private System.Windows.Forms.ListBox OrdersListBox;
        private System.Windows.Forms.Button LogoutButton;
        private System.Windows.Forms.DataGridView OfferedProductsDataGridView;
        private System.Windows.Forms.DataGridView SelectedProductsDataGridView;
        private System.Windows.Forms.Label BasketLabel;
        private System.Windows.Forms.Label AvailableProdcutsLabel;
    }
}