﻿using DataWebApi.Components;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using WinFormsApp.Components;

namespace WinFormsApp
{
    public static class RequestHelper
    {
        static HttpClient httpClient = new HttpClient();
        static Uri BaseUrl = new Uri(ConfigurationStorage.GetConfiguration("ServiceConnectionUrl"));

        public static T Execute<T>(HttpMethod method, string service, object data = null)
        {
            string json;

            try
            {
                var url = new Uri(BaseUrl, service);

                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = method.Method;
                request.ContentLength = 0;

                if (method != HttpMethod.Get && data != null)
                {
                    var postData = JsonConvert.SerializeObject(data);

                    byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                    request.ContentType = "application/json";

                    request.ContentLength = byteArray.Length;

                    using (var stream = request.GetRequestStream())
                    {
                        stream.Write(byteArray, 0, byteArray.Length);
                    }
                }

                var response = request.GetResponse();

                using (var stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        json = reader.ReadToEnd();
                    }
                }
            }
            catch (WebException wex)
            {
                throw ServiceException.Wrap(wex);
            }

            if (string.IsNullOrWhiteSpace(json))
            {
                return default(T);
            }

            return JsonConvert.DeserializeObject<T>(json); ;
        }

        public static void Execute(HttpMethod method, string service, object data = null)
        {
            Execute<object>(method, service, data);
        }
    }
}
