﻿namespace DataWebApi.Components
{
    public enum ErrorType
    {
        InternalError = 1000,
        UserNotFound = 1001,
        LoginFailed = 1002,
        UserAlreadyExist = 1003,
        FailedFetchUsers = 1004,
        RegistraionFailed = 2001,
        FailedFetchProducts = 3001,
        FailedAddNewProduct = 3002,
        FailedUpdateProduct = 3003,
        FailedRemoveProduct = 3004,
        FailedAddNewOrder = 4001,
        FailedFetchOrders = 4002,
        FailedRemoveOrder = 4003,
        FailedRemoveAllOrders = 4004
    }
}
