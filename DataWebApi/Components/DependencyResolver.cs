﻿using Microsoft.Extensions.DependencyInjection;
using System;

namespace DataWebApi.Components
{
    public class DependencyResolver
    {
        private readonly IServiceProvider _serviceProvider;

        private static DependencyResolver _resolver;

        public static DependencyResolver Current => _resolver;

        private DependencyResolver(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public static void Setup(IServiceProvider provider)
        {
            _resolver = new DependencyResolver(provider);
        }

        public T GetService<T>()
        {
            return _serviceProvider.GetService<T>();
        }
    }
}
