﻿using System;
using System.Net;

namespace DataWebApi.Components
{
    public class ServiceException : Exception
    {
        public static ServiceException Wrap(Exception ex, string errorMessage, int errorCode = 0, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            return new ServiceException(ex, errorMessage, errorCode, statusCode);
        }

        public static ServiceException Wrap(Exception ex, string errorMessage, ErrorType errorCode, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            return Wrap(ex, errorMessage, (int)errorCode, statusCode);
        }

        public ServiceException(Exception ex = null, string errorMessage = null, int errorCode = 0, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            ErrorCode = errorCode;
            ErrorMessage = errorMessage;
            StatusCode = statusCode;
            Exception = ex;
        }

        public ServiceException(Exception ex = null, string errorMessage = null, ErrorType errorCode = 0, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            ErrorCode = (int)errorCode;
            ErrorMessage = errorMessage;
            StatusCode = statusCode;
            Exception = ex;
        }

        public string ErrorMessage { get; }
        public int ErrorCode { get; }
        public HttpStatusCode StatusCode { get;  }
        public Exception Exception { get; }
    }
}
