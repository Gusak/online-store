﻿using AutoMapper;
using DataWebApi.Components;
using System.Collections.Generic;

namespace DataWebApi.Helpers
{
    public static class MapperHelper
    {
        public static T Map<T>(object obj)
        {
            var mapper = DependencyResolver.Current.GetService<IMapper>();
            return mapper.Map<T>(obj);
        }

        public static IEnumerable<T> MapCollection<T>(IEnumerable<object> list)
        {
            foreach (var item in list)
            {
                yield return Map<T>(item);
            }
        }
    }
}
