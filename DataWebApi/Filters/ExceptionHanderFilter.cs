﻿using DataWebApi.Components;
using DataWebApi.DTO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DataWebApi.Filters
{
    public class ExceptionHanderFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            HttpStatusCode status = HttpStatusCode.InternalServerError;
            String message = String.Empty;
            ServiceException serviceException;

            var exceptionType = context.Exception.GetType();
            if (exceptionType == typeof(ServiceException))
            {
                serviceException = (ServiceException)context.Exception;
            }
            else
            {
                serviceException = ServiceException.Wrap(context.Exception, errorCode: ErrorType.InternalError, errorMessage: context.Exception.Message, statusCode: status);
                message = context.Exception.Message;
            }
            context.ExceptionHandled = true;

            HttpResponse response = context.HttpContext.Response;
            response.StatusCode = (int)serviceException.StatusCode;
            response.ContentType = "application/json";

            var dto = new ServiceExceptionDto()
            {
                ErrorCode = serviceException.ErrorCode,
                ErrorMessage = serviceException.ErrorMessage
            };

            var json = JsonConvert.SerializeObject(dto);

            response.WriteAsync(json);
        }
    }
}
