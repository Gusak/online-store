﻿using System.ComponentModel.DataAnnotations;

namespace DataWebApi.DTO
{
    public class RegistrationDto
    {
        [Required]
        public string Firstname { get; set; }

        [Required]
        public string Secondname { get; set; }
    
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
