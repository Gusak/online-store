﻿using System.ComponentModel.DataAnnotations;

namespace DataWebApi.DTO
{
    public class OrderDetailsDto
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        public ProductDto Product { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int Quantity { get; set; }
    }
}
