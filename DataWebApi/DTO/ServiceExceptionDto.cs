﻿namespace DataWebApi.DTO
{
    public class ServiceExceptionDto
    {
        public int ErrorCode { get; set; }

        public string ErrorMessage { get; set; }
    }
}
