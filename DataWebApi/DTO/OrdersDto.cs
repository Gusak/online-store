﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataWebApi.DTO
{
    public class OrdersDto
    {
        [Required]
        public int? OrderId { get; set; }

        [Required]
        public IList<OrderDetailsDto> OrdersDetails { get; set; }
    }
}
