﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace DataWebApi.Extensions
{
    public static class CollectionExtension
    {
        public static string ToJson<T>(this IList<T> list)
        {
            return list != null ? JsonConvert.SerializeObject(list) : string.Empty;
        }

        public static string ToJson<T>(this IEnumerable<T> list)
        {
            return list != null ? JsonConvert.SerializeObject(list) : string.Empty;
        }
    }
}
