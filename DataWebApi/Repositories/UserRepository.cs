﻿using Dapper;
using DataWebApi.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataWebApi.Repositories
{
    public interface IUserRepository
    {
        void Add(User user);
        void Delete(int id);
        User Get(int id);
        User Get(string email, string password);
        IList<User> GetUsers();
        void Update(User user);
        bool IsUserExist(string email);
    }

    public class UserRepository : IUserRepository
    {
        private readonly string ConnectionString;

        public UserRepository(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("StoreDBConnectionString");
        }

        public void Add(User user)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                var sqlQuery = "INSERT INTO @Table (FirstName, SecondName, Email, Password) VALUES(@FirstName, @SecondName, @Email, @Password)";
                db.Execute(sqlQuery, user);
            }
        }

        public void Delete(int id)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                var sqlQuery = "DELETE FROM Users WHERE Id = @id";
                db.Execute(sqlQuery, new { id });
            }
        }

        public User Get(int id)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                return db.Query<User>("SELECT * FROM Users Where Id = @Id", new { id }).FirstOrDefault();
            }
        }

        public User Get(string email, string password)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                return db.Query<User>("SELECT * FROM Users Where Email = @Email AND Password = @Password", new { email, password }).FirstOrDefault();
            }
        }

        public IList<User> GetUsers()
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                return db.Query<User>("SELECT * FROM Users").ToList();
            }
        }

        public bool IsUserExist(string email)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                return db.Query<User>("SELECT * FROM Users Where Email = @Email", new { email }).FirstOrDefault() != null;
            }
        }

        public void Update(User user)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                var sqlQuery = "UPDATE Users SET FirstName = @FirstName, SecondName = @SecondName WHERE Id = @Id";
                db.Execute(sqlQuery, user);
            }
        }
    }
}
