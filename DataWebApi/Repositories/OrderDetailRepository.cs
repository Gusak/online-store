﻿using Dapper;
using DataWebApi.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataWebApi.Repositories
{
    public interface IOrderDetailRepository
    {
        void Add(OrderDetail orderDetail);
        void Delete(int id);
        OrderDetail Get(int id);
        IList<OrderDetail> GetOrderDetails();
        IList<OrderDetail> GetOrderDetailsByOrder(int OrderId);
        IList<OrderDetail> GetOrderDetailsByProduct(int ProductId);
        void Update(OrderDetail orderDetail);
    }

    public class OrderDetailRepository : IOrderDetailRepository
    {
        private readonly string ConnectionString;

        public OrderDetailRepository(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("StoreDBConnectionString");
        }

        public void Add(OrderDetail orderDetail)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                var sqlQuery = "INSERT INTO OrderDetails (OrderId, ProductId, Quantity) VALUES(@OrderId, @ProductId, @Quantity)";
                db.Execute(sqlQuery, orderDetail);
            }
        }

        public void Delete(int id)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                var sqlQuery = "DELETE FROM OrderDetails WHERE Id = @id";
                db.Execute(sqlQuery, new { id });
            }
        }

        public OrderDetail Get(int id)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                return db.Query<OrderDetail>("SELECT * FROM OrderDetails Where = @Id", new { id }).FirstOrDefault();
            }
        }

        public IList<OrderDetail> GetOrderDetails()
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                return db.Query<OrderDetail>("SELECT * FROM OrderDetails").ToList();
            }
        }

        public IList<OrderDetail> GetOrderDetailsByOrder(int OrderId)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                return db.Query<OrderDetail>("SELECT * FROM OrderDetails WHERE OrderId = @OrderId", new { OrderId }).ToList();
            }
        }

        public IList<OrderDetail> GetOrderDetailsByProduct(int ProductId)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                return db.Query<OrderDetail>("SELECT * FROM OrderDetails WHERE ProductId = @ProductId", new { ProductId }).ToList();
            }
        }

        public void Update(OrderDetail orderDetail)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                var sqlQuery = "UPDATE OrderDetails SET OrderId = @OrderId, ProductId = @ProductId, Quantity = @Quantity WHERE Id = @Id";
                db.Execute(sqlQuery, orderDetail);
            }
        }
    }
}
