﻿using Dapper;
using DataWebApi.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataWebApi.Repositories
{
    public interface IProductRepository
    {
        void Add(Product product);
        void Delete(int id);
        Product Get(int id);
        IList<Product> GetProducts();
        void Update(Product product);
    }

    public class ProductRepository : IProductRepository
    {
        private readonly string ConnectionString;

        public ProductRepository(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("StoreDBConnectionString");
        }

        public void Add(Product product)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                var sqlQuery = "INSERT INTO Products (Name, Price) VALUES(@Name, @Price)";
                db.Execute(sqlQuery, product);
            }
        }

        public void Delete(int id)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                var sqlQuery = "DELETE FROM Products WHERE Id = @id";
                db.Execute(sqlQuery, new { id });
            }
        }

        public Product Get(int id)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            { 
                return db.Query<Product>("SELECT * FROM Products WHERE Id = @Id", new { id }).FirstOrDefault();
            }
        }

        public IList<Product> GetProducts()
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                return db.Query<Product>("SELECT * FROM Products").ToList();
            }
        }

        public void Update(Product product)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                var sqlQuery = "UPDATE Products SET Name = @Name, Price = @Price  WHERE Id = @Id";
                db.Execute(sqlQuery, product);
            }
        }
    }
}
