﻿using DataWebApi.DTO;
using DataWebApi.Helpers;
using DataWebApi.Models;
using System.Collections.Generic;
using System.Linq;

namespace DataWebApi.Repositories
{
    public interface IRepositoryFacade
    {
        IUserRepository UserRepository { get; }
        IProductRepository ProductRepository { get; }
        IOrderRepository OrderRepository { get; }
        IOrderDetailRepository OrderDetailRepository { get; }

        void AddOrder(IList<OrderDetailsDto> orderDetails);

        IList<OrdersDto> GetAllOrders(int UserId);

        void RemoveOrder(int OrderId);

        void RemoveAllOrders(int UserId);

        void RemoveProduct(int ProductId);
    }

    public class RepositoryFacade : IRepositoryFacade
    {
        private readonly IUserRepository _userRepository;
        private readonly IProductRepository _productRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IOrderDetailRepository _orderDetailRepository;

        public IUserRepository UserRepository { get { return _userRepository; } }
        public IProductRepository ProductRepository { get { return _productRepository; } }
        public IOrderRepository OrderRepository { get { return _orderRepository; } }
        public IOrderDetailRepository OrderDetailRepository { get { return _orderDetailRepository; } }


        public RepositoryFacade(
            IUserRepository userRepository, 
            IProductRepository productRepository,
            IOrderRepository orderRepository,
            IOrderDetailRepository orderDetailRepository)
        {
            _userRepository = userRepository;
            _productRepository = productRepository;
            _orderRepository = orderRepository;
            _orderDetailRepository = orderDetailRepository;
    }

        public void AddOrder(IList<OrderDetailsDto> orderDetails)
        {
            var details = MapperHelper.Map<List<OrderDetail>>(orderDetails);

            var newOrder = new Order
            {
                UserId = orderDetails.FirstOrDefault().UserId
            };

            var orderId = _orderRepository.Add(newOrder);

            foreach (var detail in details)
            {
                detail.OrderId = orderId;
                _orderDetailRepository.Add(detail);
            }
        }

        public IList<OrdersDto> GetAllOrders(int UserId)
        {
            var orders = _orderRepository.GetOrdersByUser(UserId);
            var result = new List<OrdersDto>();
            foreach (var order in orders)
            {
                var details = _orderDetailRepository.GetOrderDetailsByOrder(order.Id);
                if (details != null && details.Count() > 0)
                {
                    var listOrderDetailsDto = new List<OrderDetailsDto>();

                    foreach(var detail in details)
                    {
                        listOrderDetailsDto.Add(new OrderDetailsDto
                        {
                            Product = MapperHelper.Map<ProductDto>(_productRepository.Get(detail.ProductId)),
                            Quantity = detail.Quantity,
                            UserId = order.Id
                        });
                    }

                    result.Add(new OrdersDto
                    {
                        OrderId = order.Id,
                        OrdersDetails = listOrderDetailsDto
                    });
                }
            }

            return result;
        }

        public void RemoveAllOrders(int UserId)
        {
            var orders = _orderRepository.GetOrdersByUser(UserId);
            var ordersDetails = new List<OrderDetail>();

            foreach (var order in orders)
            {
                var orderDetails = _orderDetailRepository.GetOrderDetailsByOrder(order.Id);
                if (orderDetails != null)
                {
                    ordersDetails.AddRange(orderDetails);
                }
            }

            foreach(var detail in ordersDetails)
            {
                _orderDetailRepository.Delete(detail.Id);
            }

            foreach (var order in orders)
            {
                _orderRepository.Delete(order.Id);
            }
        }

        public void RemoveOrder(int OrderId)
        {
            var orderDetails = _orderDetailRepository.GetOrderDetailsByOrder(OrderId);
            if (orderDetails != null && orderDetails.Count > 0)
            {
                foreach (var detail in orderDetails)
                {
                    _orderDetailRepository.Delete(detail.Id);
                }
            }

            _orderRepository.Delete(OrderId);
        }

        public void RemoveProduct(int ProductId)
        {
            var orderDetails = _orderDetailRepository.GetOrderDetailsByProduct(ProductId);
            if (orderDetails != null && orderDetails.Count > 0)
            {
                foreach(var detail in orderDetails)
                {
                    _orderDetailRepository.Delete(detail.Id);
                }
            }

            _productRepository.Delete(ProductId);
        }
    }
}
