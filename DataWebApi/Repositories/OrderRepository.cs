﻿using Dapper;
using DataWebApi.Models;
using Microsoft.Extensions.Configuration;
using Npgsql;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace DataWebApi.Repositories
{
    public interface IOrderRepository
    {
        int Add(Order order);
        void Delete(int id);
        Order Get(int id);
        IList<Order> GetOrders();
        IList<Order> GetOrdersByUser(int UserId);
        void Update(Order order);
    }

    public class OrderRepository : IOrderRepository
    {
        private readonly string ConnectionString;

        public OrderRepository(IConfiguration configuration)
        {
            ConnectionString = configuration.GetConnectionString("StoreDBConnectionString");
        }

        public int Add(Order order)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                var sqlQuery = "INSERT INTO Orders (UserId) VALUES(@UserId) RETURNING Id";
                return db.Query<int>(sqlQuery, order).FirstOrDefault();
            }
        }

        public void Delete(int id)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                var sqlQuery = "DELETE FROM Orders WHERE Id = @id";
                db.Execute(sqlQuery, new { id });
            }
        }

        public Order Get(int id)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                return db.Query<Order>("SELECT * FROM Orders Where Id = @Id", new { id }).FirstOrDefault();
            }
        }

        public IList<Order> GetOrders()
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                return db.Query<Order>("SELECT * FROM Orders").ToList();
            }
        }

        public IList<Order> GetOrdersByUser(int UserId)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                return db.Query<Order>("SELECT * FROM Orders WHERE UserId = @UserId", new { UserId }).ToList();
            }
        }

        public void Update(Order order)
        {
            using (IDbConnection db = new NpgsqlConnection(ConnectionString))
            {
                var sqlQuery = "UPDATE Orders SET UserId = @UserId WHERE Id = @Id";
                db.Execute(sqlQuery, order);
            }
        }
    }
}
