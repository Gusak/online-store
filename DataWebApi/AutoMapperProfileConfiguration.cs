﻿using AutoMapper;
using DataWebApi.DTO;
using DataWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataWebApi
{
    public class AutoMapperProfileConfiguration : Profile
    {
        public AutoMapperProfileConfiguration() : this("MyProfile")
        {
        }
        protected AutoMapperProfileConfiguration(string profileName)
        : base(profileName)
        {
            CreateMap<LoginDto, User>().ReverseMap();
            CreateMap<RegistrationDto, User>().ReverseMap();
            CreateMap<OrdersDto, Order>().ReverseMap();
            CreateMap<ProductDto, Product>().ReverseMap();
            CreateMap<OrderDetailsDto, OrderDetail>()
                            .ForMember(c => c.ProductId, opts => opts.MapFrom(src => src.Product.Id));

            CreateMap<Product, Product>().ForPath(x => x.Id, x => x.Ignore());
            CreateMap<User, User>().ForPath(x => x.Id, x => x.Ignore());
            CreateMap<OrderDetail, OrderDetail>().ForPath(x => x.Id, x => x.Ignore());
            CreateMap<Order, Order>().ForPath(x => x.Id, x => x.Ignore());

            //CreateMap<OrderDetail, OrderDetailsDto>()
            //    .ForMember(c => c.Product, opts => opts.MapFrom(src => src.Product));
        }
    }
}
