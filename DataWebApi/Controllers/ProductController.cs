﻿using DataWebApi.Models;
using Microsoft.AspNetCore.Mvc;
using DataWebApi.DTO;
using DataWebApi.Extensions;
using DataWebApi.Helpers;
using DataWebApi.Repositories;
using System;
using DataWebApi.Components;
using System.Collections.Generic;
using log4net;

namespace DataWebApi.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));
        private readonly IRepositoryFacade _repositoryFacade;

        public ProductController(IRepositoryFacade repositoryFacade)
        {
            _repositoryFacade = repositoryFacade;
        }

        [HttpGet]
        [Route("all")]
        public ActionResult GetAllProducts()
        {
            IList<Product> getAllProducts;
            try
            {
                getAllProducts = _repositoryFacade.ProductRepository.GetProducts();
            }
            catch(Exception ex)
            {
                Log.ErrorFormat("Error [{0}]: The error has been occured while trying to fetch products list", ErrorType.FailedFetchProducts);
                throw ServiceException.Wrap(ex, "Error has been occured while trying to fetch products list", ErrorType.FailedFetchProducts);
            }
            return Ok(MapperHelper.MapCollection<ProductDto>(getAllProducts).ToJson());
        }

        [HttpPost]
        [Route("add")]
        public ActionResult AddProduct([FromBody]ProductDto request)
        {
            try
            {
                var product = MapperHelper.Map<Product>(request);
                _repositoryFacade.ProductRepository.Add(product);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error [{0}]: The error has been occured while trying to add new product", ErrorType.FailedAddNewProduct);
                throw ServiceException.Wrap(ex, "Error has been occured while trying to add new product", ErrorType.FailedAddNewProduct);
            }

            return Ok();
        }

        [HttpPut]
        [Route("update")]
        public ActionResult UpdateProduct([FromBody]ProductDto request)
        {
            try
            {
                var product = MapperHelper.Map<Product>(request);
                _repositoryFacade.ProductRepository.Update(product);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error [{0}]: The error has been occured while trying to update product", ErrorType.FailedUpdateProduct);
                throw ServiceException.Wrap(ex, "Error has been occured while trying to update product", ErrorType.FailedUpdateProduct);
            }

            return Ok();
        }

        [HttpDelete]
        [Route("remove")]
        public ActionResult RemoveProduct([FromBody]ProductDto request)
        {
            try
            {
                _repositoryFacade.RemoveProduct(request.Id);
            }
            catch (Exception ex)
            {
                throw ServiceException.Wrap(ex, "Error has been occured while trying to remove product", ErrorType.FailedRemoveProduct);
            }

            return Ok();
        }
    }
}
