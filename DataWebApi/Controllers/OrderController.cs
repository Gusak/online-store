﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using DataWebApi.DTO;
using DataWebApi.Extensions;
using DataWebApi.Repositories;
using log4net;
using System;
using DataWebApi.Components;

namespace DataWebApi.Controllers
{
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));
        private readonly IRepositoryFacade _repositoryFacade;
        public OrderController(IRepositoryFacade repositoryFacade)
        {
            _repositoryFacade = repositoryFacade;
        }

        [HttpPost]
        [Route("add")]
        public ActionResult AddOrder([FromBody]IList<OrderDetailsDto> orderDetails)
        {
            try
            {
                _repositoryFacade.AddOrder(orderDetails);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error [{0}]: The error has been occured while trying to update product", ErrorType.FailedUpdateProduct);
                throw ServiceException.Wrap(ex, "Error has been occured while trying to add new order", ErrorType.FailedAddNewOrder);
            }
            return Ok();
        }

        [HttpPost]
        [Route("all")]
        public ActionResult GetAllOrders([FromBody]int UserId)
        {
            string result;
            try
            {
                result =_repositoryFacade.GetAllOrders(UserId).ToJson(); ;
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error [{0}]: The error has been occured while trying to fetch user's orders", ErrorType.FailedFetchOrders);
                throw ServiceException.Wrap(ex, "Error has been occured while trying to get user's order list", ErrorType.FailedFetchOrders);
            }
            return Ok(result);
        }

        [HttpDelete]
        [Route("remove")]
        public ActionResult RemoveOrder([FromBody]int OrderId)
        {
            try
            {
                _repositoryFacade.RemoveOrder(OrderId);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error [{0}]: The error has been occured while trying to remove order", ErrorType.FailedRemoveOrder);
                throw ServiceException.Wrap(ex, "Error has been occured while trying to remove order", ErrorType.FailedRemoveOrder);
            }
            return Ok();
        }

        [HttpDelete]
        [Route("removeall")]
        public ActionResult RemoveAll([FromBody]int UserId)
        {
            try
            {
                _repositoryFacade.RemoveAllOrders(UserId);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error [{0}]: The error has been occured while trying to remove all user's orders", ErrorType.FailedRemoveAllOrders);
                throw ServiceException.Wrap(ex, "Error has been occured while trying to remove all user's orders", ErrorType.FailedRemoveAllOrders);
            }
            return Ok();
        }
    }
}
