﻿using DataWebApi.Models;
using Microsoft.AspNetCore.Mvc;
using DataWebApi.DTO;
using DataWebApi.Extensions;
using DataWebApi.Helpers;
using DataWebApi.Components;
using System;
using DataWebApi.Repositories;
using log4net;

namespace DataWebApi.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));
        private readonly IRepositoryFacade _repositoryFacade;

        public UserController(IRepositoryFacade repositoryFacade)
        {
            _repositoryFacade = repositoryFacade;
        }

        [HttpGet]
        [Route("all")]
        public ActionResult GetUsers()
        {
            string result;
            try
            {
                result = _repositoryFacade.UserRepository.GetUsers().ToJson();
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error [{0}]: The error has been occured while trying to fetch users list", ErrorType.FailedFetchUsers);
                throw ServiceException.Wrap(ex, "Error has been occured while trying to fetch users list", ErrorType.FailedFetchUsers);
            }
            return Ok(result);
        }

        [HttpPost]
        [Route("register")]
        public ActionResult RegisterUser([FromBody]RegistrationDto request)
        {
            var user = MapperHelper.Map<User>(request);

            if (_repositoryFacade.UserRepository.IsUserExist(request.Email))
                throw new ServiceException(errorMessage: "User already exists", errorCode: ErrorType.UserAlreadyExist);

            try
            {
                _repositoryFacade.UserRepository.Add(user);
            }
            catch(Exception ex)
            {
                Log.ErrorFormat("Error [{0}]: The error has been occured while trying to register new user", ErrorType.RegistraionFailed);
                throw ServiceException.Wrap(ex, "Registration failed", ErrorType.RegistraionFailed);
            }

            return Ok();
        }

        [HttpPost]
        [Route("login")]
        public ActionResult Login([FromBody]LoginDto request)
        {
            User result;
            try
            {
                result = _repositoryFacade.UserRepository.Get(request.Email, request.Password);
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error [{0}]: The error has been occured while trying to login", ErrorType.LoginFailed);
                throw ServiceException.Wrap(ex, "Login failed", ErrorType.LoginFailed);
            }

            return Ok(result);
        }
    }
}
